//make a route to get single course step 3 (creating html single course.html)
//make a route to get single course step 4 (single course.js)
console.log(window.location.search); 
let params = new URLSearchParams(window.location.search);

//method of URLSearchParams
	// URLSearchParams.get()
let courseId = params.get('courseId')
console.log(courseId);

let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let enrollmentCountainer = document.querySelector('#enrollmentContainer')

let token = localStorage.getItem('token');

fetch(`http://localhost:3000/api/courses/${courseId}`, {
	method: "GET",
	headers: {
		"Authorization": `Bearer ${token}`
	}

} 
).then(result => result.json())
.then( result => {
	console.log(result) // single course document// object
	courseName.innerHTML = result.name
	courseDesc.innerHTML = result.description
	coursePrice.innerHTML = result.price
	enrollmentCountainer.innerHTML = 
	`
	<button class = "btn btn-primary btn-block"> Enroll </button>
	`
})