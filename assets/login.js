
let loginUser = document.querySelector('#loginUser');

loginUser.addEventListener("submit", (event) => {

	event.preventDefault();

let email = document.querySelector('#email').value
let password = document.querySelector('#password').value
	
	if(email == "" || password == ""){
		alert(`Please input required fields`)
	} else {
		fetch('http://localhost:3000/api/users/login', 
		{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}
		).then(result => result.json())
		.then(result => {

			//localStorage.setItem(<key>, <value>);

			localStorage.setItem("token", result.access);

			if (result.access){
				fetch('http://localhost:3000/api/users/details', 
					{
						method: "GET",
						headers: {
							"Authorization": `Bearer ${result.access}`
						}
					}
				)
				.then(result => result.json())
				.then(result => {
					
					console.log(result)

					/*
					mini activity
					1. save id, isAdmin to local storage
					2. transfer to courses.html
				*/

					localStorage.setItem("id", result._id);
					localStorage.setItem("isAdmin", result.isAdmin);

					window.location.replace('./courses.html')


				})

				


			}
		})
	}

})
